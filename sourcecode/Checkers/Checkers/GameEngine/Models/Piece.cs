﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.GameEngine.Models
{
    class Piece
    {
        private int pieceIndex { get; set; }

        private int xCoor { get; set; }
        private int yCoor { get; set; }

        public enum colour
        {
            Red,
            White
        }

        private colour Colour { get; set; }
    }
}
