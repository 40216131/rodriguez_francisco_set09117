﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.GameEngine.Models
{
    class Movement
    {
        private int originalXCoor { get; set; }
        private int originalYCoor { get; set; }

        private int finalXCoor { get; set; }
        private int finalYCoor { get; set; }

        private List<Piece> capturedPieces { get; set; }
    }
}
